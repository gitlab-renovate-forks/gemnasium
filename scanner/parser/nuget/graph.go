package nuget

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// graph represents the package and dependency structure of a nuget lockfile
type graph struct {
	named     map[string]*node // named maps package nodes by the package name
	versioned map[string]*node // versioned maps package nodes by the package name and version
}

type node struct {
	pkg       parser.Package   // pkg the parser.Package representation of the nuget package
	nodeType  nodeType         // nodeType denotes one of the 3 types of nodes possible in a nuget lockfile
	requested string           // requested is the version or range specified (as opposed to what was resolved)
	edges     map[string]*edge // edges is the dependency relationship
}

type edge struct {
	to        *node  // to is the package being included as a dependency
	requested string // requested is the version or range specified (as opposed to what was resolved)
}

func newGraph() graph {
	return graph{
		named:     make(map[string]*node),
		versioned: make(map[string]*node),
	}
}

// create a unique key for a package for its name
func nameKey(name string) string {
	return strings.ToLower(fmt.Sprintf("%s", name))
}

// create a unique key for a package using its name and version
func nameVersionKey(name, version string) string {
	return strings.ToLower(fmt.Sprintf("%s@%s", name, version))
}

// add adds a particular parse.Package to the graph as a node
// the node is then mapped both by name and by name version pair
// nodes mapped by name and version are used to get the exact packages which was resolved by nuget
// nodes mapped by name are used to retrieve the same resolved dependency each time
func (ns *graph) add(pkg parser.Package, ntype nodeType, requested string) {
	n := &node{pkg: pkg, nodeType: ntype, requested: requested, edges: make(map[string]*edge)}
	ns.named[nameKey(pkg.Name)] = n
	ns.versioned[nameVersionKey(pkg.Name, pkg.Version)] = n
}

// addEdge adds the relationship between a node and its dependency and is keyed by the dependency's package name
func (ns *graph) addEdge(from, to *node, requested string) {
	key := nameKey(to.pkg.Name)
	if _, ok := from.edges[key]; !ok {
		from.edges[key] = &edge{
			to:        to,
			requested: requested,
		}
	}
}

func (ns *graph) findByName(name string) (*node, error) {
	n, found := ns.named[nameKey(name)]
	if !found {
		return nil, fmt.Errorf("node with package name %s does not exist", name)
	}
	return n, nil
}

func (ns *graph) findByVersion(name, ver string) (*node, error) {
	n, found := ns.versioned[nameVersionKey(name, ver)]
	if !found {
		return nil, fmt.Errorf("node with package name %s and version %s does not exist", name, ver)
	}
	return n, nil
}

func (ns *graph) packages() []parser.Package {
	pkgs := []parser.Package{}
	for _, n := range ns.versioned {
		if n.nodeType == nodeTypeProject {
			continue
		}
		pkgs = append(pkgs, n.pkg)
	}
	return pkgs
}

func (ns *graph) dependencies() []parser.Dependency {
	var deps []parser.Dependency
	for _, n := range ns.versioned {
		if n.nodeType == nodeTypeDirect {
			dep := parser.Dependency{
				To:           &n.pkg,
				VersionRange: n.requested,
			}
			deps = append(deps, dep)
		}
		for _, e := range n.edges {
			dep := parser.Dependency{
				From:         &n.pkg,
				To:           &e.to.pkg,
				VersionRange: e.requested,
			}
			deps = append(deps, dep)
		}
	}
	return deps
}
