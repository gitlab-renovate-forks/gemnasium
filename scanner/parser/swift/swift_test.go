package swift

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestParseSwift(t *testing.T) {
	testCases := []struct {
		fixturePath string
		lockfile    string
	}{
		{
			fixturePath: "v1",
			lockfile:    "Package.resolved",
		},
		{
			fixturePath: "v2",
			lockfile:    "Package.resolved",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.lockfile, func(t *testing.T) {
			fixture := testutil.Fixture(t, tc.fixturePath, tc.lockfile)
			dependencies, _, err := Parse(fixture, parser.Options{})
			require.NoError(t, err)

			testutil.RequireExpectedPackages(t, tc.fixturePath, dependencies)
		})
	}
}
