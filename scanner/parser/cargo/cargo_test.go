package cargo

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestCargo(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir     string
			expectationDir string
			opts           parser.Options
		}{
			"simple": {
				fixtureDir:     "simple",
				expectationDir: "simple",
			},
			"big": {
				fixtureDir:     "big",
				expectationDir: "big",
			},
		}
		for name, tt := range tests {
			tt := tt
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "Cargo.lock")
				pkgs, deps, err := Parse(fixture, tt.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tt.expectationDir, pkgs)
				testutil.RequireExpectedDependencies(t, tt.expectationDir, deps)
			})
		}
	})
	t.Run("Errors", func(t *testing.T) {
		tests := map[string]struct {
			lockfileContents string
			errstr           string
		}{
			"toml_parsing_error": {
				lockfileContents: `
					package]]
					name = "application"
					version = "v1"
				`,
				errstr: "unmarshaling Cargo.lock: toml: line 2: expected '.' or '=', but got ']' instead",
			},
			"unknown_package_in_dependencies_1": {
				lockfileContents: `
					[[package]]
					name = "foo"
					version = "v2"

					[[package]]
					name = "application"
					version = "v1"
					dependencies = [
					 "bar",
					]
				`,
				errstr: "dependencies list refers to an unknown package name bar",
			},
			"unknown_package_in_dependencies_2": {
				lockfileContents: `
					[[package]]
					name = "foo"
					version = "v2"

					[[package]]
					name = "application"
					version = "v1"
					dependencies = [
					 "bar v2",
					]
				`,
				errstr: "dependencies list refers to an unknown package name bar",
			},
			"unreferenced top-level package": {
				lockfileContents: `
					[[package]]
					name = "foo"
					version = "v2"

					[[package]]
					name = "application"
					version = "v1"
					dependencies = [
					 "foo",
					]

					[[package]]
					name = "foo"
					version = "v3"
				`,
				errstr: "lockfile should only have one instance of package foo",
			},
			"dependencies list refers to an unknown verison of a package": {
				lockfileContents: `
					[[package]]
					name = "foo"
					version = "v2"

					[[package]]
					name = "application"
					version = "v1"
					dependencies = [
					 "foo v1",
					]
				`,
				errstr: "dependencies list refers to an unknown package version foo@v1",
			},
		}
		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				lockfile := strings.NewReader(tt.lockfileContents)
				_, _, err := Parse(lockfile, parser.Options{})
				require.ErrorContains(t, err, tt.errstr)
			})

		}
	})
}
