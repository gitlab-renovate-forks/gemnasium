package gradle

import (
	"fmt"
	"strings"
)

const (
	// nonePolicy considers everything as resolved, allowing partial results.
	nonePolicy = "none"
	// failedPolicy considers only failed resolutions as unresolved.
	failedPolicy = "failed"
	// defaultPolicy is the fallback policy used when an empty string is given.
	defaultPolicy = failedPolicy
)

// resolutionPolicy defines a collection of resolution states that should result
// in an error.
type resolutionPolicy struct {
	states []string
}

// newResolutionPolicy creates a new policy based on the environment variable.
func newResolutionPolicy(policy string) (*resolutionPolicy, error) {
	if policy == "" {
		policy = defaultPolicy
	}

	var states []string
	switch strings.ToLower(policy) {
	case nonePolicy:
		states = []string{}
	case failedPolicy:
		states = []string{"FAILED"}
	default:
		return nil, fmt.Errorf("unknown resolution policy: %s", policy)
	}

	return &resolutionPolicy{states: states}, nil
}

// violation checks if the given state is part of the ResolutionPolicy states.
func (p *resolutionPolicy) violation(state string) bool {
	for _, s := range p.states {
		if strings.EqualFold(s, state) {
			return true
		}
	}

	return false
}
