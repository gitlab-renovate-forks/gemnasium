package gradle

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestGradle(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tcs := []struct {
			name           string
			fixture        string
			fixtureDir     string
			expectationDir string
			expectedError  error
		}{
			{
				name:           "root in multimodule project",
				fixture:        "root.js",
				fixtureDir:     "multimodules/build/reports/project/dependencies",
				expectationDir: "root",
			},
			{
				name:           "child in multimodule project",
				fixture:        "root.api.js",
				fixtureDir:     "multimodules/api/build/reports/project/dependencies",
				expectationDir: "root-api",
			},
			{
				name:           "project with version conflicts",
				fixture:        "root.js",
				fixtureDir:     "conflict-resolution/build/reports/project/dependencies",
				expectationDir: "conflict-resolution",
			},
			{
				name:          "project with dependencies that failed to resolve",
				fixture:       "root.js",
				fixtureDir:    "unresolvable/build/reports/project/dependencies",
				expectedError: fmt.Errorf(`project has unresolved dependencies: ["doesnot:exist:1.0"]. See troubleshooting steps: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/troubleshooting_dependency_scanning.html#error-project-has-unresolved-dependencies`),
			},
		}

		for _, tc := range tcs {
			t.Run(tc.fixture, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.fixtureDir, tc.fixture)
				got, _, err := Parse(fixture, parser.Options{})

				if tc.expectedError != nil {
					require.Error(t, err)
					require.EqualError(t, err, tc.expectedError.Error())
				} else {
					require.NoError(t, err)
					testutil.RequireExpectedPackages(t, tc.expectationDir, got)
				}
			})
		}
	})
}

func TestGradleParse_NewResolutionPolicyError(t *testing.T) {
	fixture := strings.NewReader(`var projectDependencyReport = {"project": {"configurations": []}};`)
	_, _, err := Parse(fixture, parser.Options{GradleResolutionPolicy: "unknown-policy"})

	require.Error(t, err)
	require.EqualError(t, err, "gradle parser: unknown resolution policy: unknown-policy")
}
