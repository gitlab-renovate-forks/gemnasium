package uv

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestUV(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir     string
			expectationDir string
			opts           parser.Options
		}{
			"simple": {
				fixtureDir:     "simple",
				expectationDir: "simple",
				opts:           parser.Options{IncludeDev: true},
			},
			"simple_no_dev": {
				fixtureDir:     "simple",
				expectationDir: "simple_no_dev",
				opts:           parser.Options{IncludeDev: false},
			},
		}

		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "uv.lock")
				got, _, err := Parse(fixture, tt.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tt.expectationDir, got)
			})
		}
	})

	t.Run("Parse_Errors", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir string
			opts       parser.Options
			err        error
		}{
			"missing_version": {
				fixtureDir: "missing_version",
				err:        parser.ErrWrongFileFormatVersion,
				opts:       parser.Options{IncludeDev: true},
			},
		}
		for name, tt := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "uv.lock")
				_, _, err := Parse(fixture, tt.opts)
				require.Equal(t, err, tt.err)
			})
		}
	})
}
