package gradle

import (
	"flag"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBuilder(t *testing.T) {
	t.Run("returns the expected default values", func(t *testing.T) {
		b := &Builder{}
		set := flag.NewFlagSet("test", 0)

		tcs := []struct {
			flagName             string
			expectedDefaultValue string
		}{
			{
				flagGradleOpts,
				"",
			},
			{
				flagGradleInitScript,
				"gemnasium-init.gradle",
			},
		}

		for _, flag := range b.Flags() {
			flag.Apply(set)
		}

		for _, tc := range tcs {
			f := set.Lookup(tc.flagName)
			require.Equal(t, f.DefValue, tc.expectedDefaultValue)
		}
	})
}
