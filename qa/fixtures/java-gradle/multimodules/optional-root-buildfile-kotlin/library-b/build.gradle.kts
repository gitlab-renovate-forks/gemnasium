plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":internal-module"))
    implementation("org.apache.geode:geode-core:1.1.1")
}
