{
  "version": "15.0.4",
  "vulnerabilities": [
    {
      "id": "b334d03be2b917af1f0eca2deae30c237e5ecd9b04833abf5a7d38bc0b984b06",
      "name": "Improper Neutralization of Special Elements in Output Used by a Downstream Component ('Injection')",
      "description": "It was found that the fix to address CVE-2021-44228 in Apache Log4j was incomplete in certain non-default configurations. This could allows attackers with control over Thread Context Map (MDC) input data when the logging configuration uses a non-default Pattern Layout with either a Context Lookup (for example, `$${ctx:loginId})` or a Thread Context Map pattern (`%X`, `%mdc`, or `%MDC`) to craft malicious input data using a JNDI Lookup pattern resulting in a denial of service (DOS) attack. Log4j restricts JNDI LDAP lookups to localhost by default. Note that previous mitigations involving configuration such as to set the system property `log4j2.noFormatMsgLookup` to `true` do NOT mitigate this specific vulnerability. Log4j fixes this issue by removing support for message lookup patterns and disabling JNDI functionality by default. This issue can be mitigated in prior releases (<2.16.0) by removing the JndiLookup class from the classpath (example: zip -q -d log4j-core-*.jar org/apache/logging/log4j/core/lookup/JndiLookup.class).",
      "severity": "Critical",
      "solution": "Upgrade to versions 2.12.3, 2.16.0 or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "org.apache.logging.log4j/log4j-core"
          },
          "version": "2.13.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-788f3d07-b01c-41d3-b9f0-56693bb21ec8",
          "value": "788f3d07-b01c-41d3-b9f0-56693bb21ec8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v2.0.749/maven/org.apache.logging.log4j/log4j-core/CVE-2021-45046.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2021-45046",
          "value": "CVE-2021-45046",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046"
        }
      ],
      "links": [
        {
          "url": "http://www.openwall.com/lists/oss-security/2021/12/14/4"
        },
        {
          "url": "https://logging.apache.org/log4j/2.x/security.html"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-45046"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "org.apache.logging.log4j/log4j-core:2.13.2"
        }
      }
    },
    {
      "id": "a0ced3f1680d37c33f2cc95190747a6fa5fd3e210e11ee1033b108a0c0362311",
      "name": "Improper Neutralization of Special Elements in Output Used by a Downstream Component ('Injection')",
      "description": "JNDI features used in configuration, log messages, and parameters do not protect against attacker controlled LDAP and other JNDI related endpoints. An attacker who can control log messages or log message parameters can execute arbitrary code loaded from LDAP servers when message lookup substitution is enabled. From log4j, this behavior has been disabled by default. In previous releases (>2.10) this behavior can be mitigated by setting system property `log4j2.formatMsgNoLookups` to `true` or it can be mitigated in prior releases (<2.10) by removing the JndiLookup class from the classpath (example, `zip -q -d log4j-core-*.jar org/apache/logging/log4j/core/lookup/JndiLookup.class`).",
      "severity": "Critical",
      "solution": "Upgrade to versions 2.3.2, 2.12.2, 2.15.0 or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "org.apache.logging.log4j/log4j-core"
          },
          "version": "2.13.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-a1a68216-be96-42ea-a47c-5e4fc4f8318c",
          "value": "a1a68216-be96-42ea-a47c-5e4fc4f8318c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v2.0.749/maven/org.apache.logging.log4j/log4j-core/CVE-2021-44228.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2021-44228",
          "value": "CVE-2021-44228",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228"
        },
        {
          "type": "ghsa",
          "name": "GHSA-jfh8-c2jp-5v3q",
          "value": "GHSA-jfh8-c2jp-5v3q",
          "url": "https://github.com/advisories/GHSA-jfh8-c2jp-5v3q"
        }
      ],
      "links": [
        {
          "url": "https://github.com/apache/logging-log4j2/pull/608#issuecomment-990494126"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-44228"
        },
        {
          "url": "https://www.lunasec.io/docs/blog/log4j-zero-day/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "org.apache.logging.log4j/log4j-core:2.13.2"
        }
      }
    },
    {
      "id": "0a99ff72e00e3e82607b74c57b9ebb7c30277538e5d6d09425f5c828a4c48aee",
      "name": "Improper Neutralization of Special Elements in Output Used by a Downstream Component ('Injection')",
      "description": "Apache Log4j2 versions 2.0-beta7 through 2.17.0 are vulnerable to a remote code execution (RCE) attack when a configuration uses a JDBC Appender with a JNDI LDAP data source URI when an attacker has control of the target LDAP server. This issue is fixed by limiting JNDI data source names to the java protocol in Log4j2.",
      "severity": "Medium",
      "solution": "Upgrade to versions 2.3.2, 2.12.4, 2.17.1 or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "org.apache.logging.log4j/log4j-api"
          },
          "version": "2.13.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-aac29170-dbdc-489f-925a-5bfc54bb0191",
          "value": "aac29170-dbdc-489f-925a-5bfc54bb0191",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v2.0.749/maven/org.apache.logging.log4j/log4j-api/CVE-2021-44832.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2021-44832",
          "value": "CVE-2021-44832",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832"
        },
        {
          "type": "ghsa",
          "name": "GHSA-8489-44mv-ggj8",
          "value": "GHSA-8489-44mv-ggj8",
          "url": "https://github.com/advisories/GHSA-8489-44mv-ggj8"
        }
      ],
      "links": [
        {
          "url": "http://www.openwall.com/lists/oss-security/2021/12/28/1"
        },
        {
          "url": "https://cert-portal.siemens.com/productcert/pdf/ssa-784507.pdf"
        },
        {
          "url": "https://github.com/advisories/GHSA-8489-44mv-ggj8"
        },
        {
          "url": "https://issues.apache.org/jira/browse/LOG4J2-3293"
        },
        {
          "url": "https://lists.apache.org/thread/s1o5vlo78ypqxnzn6p8zf6t9shtq5143"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2021/12/msg00036.html"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-44832"
        },
        {
          "url": "https://www.oracle.com/security-alerts/cpujan2022.html"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "org.apache.logging.log4j/log4j-api:2.13.2"
        }
      }
    },
    {
      "id": "5f17561077f74d01f34d055492df10ae26407084d67e8c7b804995f8dda84f4a",
      "name": "Uncontrolled Recursion",
      "description": "Apache Log4j2 versions 2.0-alpha1 through 2.16.0 (excluding 2.12.3 and 2.3.1) did not protect from uncontrolled recursion from self-referential lookups. This allows an attacker with control over Thread Context Map data to cause a denial of service when a crafted string is interpreted. This issue was fixed in Log4j 2.17.0, 2.12.3, and 2.3.1.",
      "severity": "Medium",
      "solution": "Upgrade to versions 2.12.3, 2.17.0 or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "org.apache.logging.log4j/log4j-api"
          },
          "version": "2.13.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-f2e8f66e-6588-4771-bf08-cff3bfad7a12",
          "value": "f2e8f66e-6588-4771-bf08-cff3bfad7a12",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v2.0.749/maven/org.apache.logging.log4j/log4j-api/CVE-2021-45105.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2021-45105",
          "value": "CVE-2021-45105",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105"
        },
        {
          "type": "ghsa",
          "name": "GHSA-p6xc-xr62-6r2g",
          "value": "GHSA-p6xc-xr62-6r2g",
          "url": "https://github.com/advisories/GHSA-p6xc-xr62-6r2g"
        }
      ],
      "links": [
        {
          "url": "http://www.openwall.com/lists/oss-security/2021/12/19/1"
        },
        {
          "url": "https://cert-portal.siemens.com/productcert/pdf/ssa-479842.pdf"
        },
        {
          "url": "https://cert-portal.siemens.com/productcert/pdf/ssa-501673.pdf"
        },
        {
          "url": "https://github.com/advisories/GHSA-p6xc-xr62-6r2g"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2021/12/msg00017.html"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/EOKPQGV24RRBBI4TBZUDQMM4MEH7MXCY/"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/SIG7FZULMNK2XF6FZRU4VWYDQXNMUGAJ/"
        },
        {
          "url": "https://logging.apache.org/log4j/2.x/security.html"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-45105"
        },
        {
          "url": "https://psirt.global.sonicwall.com/vuln-detail/SNWLID-2021-0032"
        },
        {
          "url": "https://security.netapp.com/advisory/ntap-20211218-0001/"
        },
        {
          "url": "https://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-apache-log4j-qRuKNEbd"
        },
        {
          "url": "https://www.debian.org/security/2021/dsa-5024"
        },
        {
          "url": "https://www.kb.cert.org/vuls/id/930724"
        },
        {
          "url": "https://www.oracle.com/security-alerts/cpujan2022.html"
        },
        {
          "url": "https://www.zerodayinitiative.com/advisories/ZDI-21-1541/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "org.apache.logging.log4j/log4j-api:2.13.2"
        }
      }
    },
    {
      "id": "2a1b106182d280ac3fcc7782308ae1b6e6e1ac854f7ee0a04c433b1e734554c6",
      "name": "Improper Input Validation",
      "description": "Apache Log4j2 ",
      "severity": "Medium",
      "solution": "Upgrade to version 2.17.1 or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "org.apache.logging.log4j/log4j-core"
          },
          "version": "2.13.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-be579f59-acaa-442b-ad7b-cf1ddea99b15",
          "value": "be579f59-acaa-442b-ad7b-cf1ddea99b15",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v2.0.749/maven/org.apache.logging.log4j/log4j-core/CVE-2021-44832.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2021-44832",
          "value": "CVE-2021-44832",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832"
        }
      ],
      "links": [
        {
          "url": "http://www.openwall.com/lists/oss-security/2021/12/28/1"
        },
        {
          "url": "https://cert-portal.siemens.com/productcert/pdf/ssa-784507.pdf"
        },
        {
          "url": "https://issues.apache.org/jira/browse/LOG4J2-3293"
        },
        {
          "url": "https://lists.apache.org/thread/s1o5vlo78ypqxnzn6p8zf6t9shtq5143"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2021/12/msg00036.html"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-44832"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "org.apache.logging.log4j/log4j-core:2.13.2"
        }
      }
    },
    {
      "id": "da6ba6f5a8c4a6d5796262bc485d58cfc98eac4bed250713d9bb8e7419d4fca6",
      "name": "Improper Input Validation",
      "description": "Apache Log4j2 does not protect from uncontrolled recursion from self-referential lookups. This allows an attacker with control over Thread Context Map data to cause a denial of service when a crafted string is interpreted.",
      "severity": "Medium",
      "solution": "Upgrade to versions 2.3.1, 2.12.3, 2.17.0 or above.",
      "location": {
        "file": "build.gradle",
        "dependency": {
          "package": {
            "name": "org.apache.logging.log4j/log4j-core"
          },
          "version": "2.13.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-d72930d1-220e-47da-8fff-1e6ad9a98ebd",
          "value": "d72930d1-220e-47da-8fff-1e6ad9a98ebd",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v2.0.749/maven/org.apache.logging.log4j/log4j-core/CVE-2021-45105.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2021-45105",
          "value": "CVE-2021-45105",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105"
        }
      ],
      "links": [
        {
          "url": "http://www.openwall.com/lists/oss-security/2021/12/19/1"
        },
        {
          "url": "https://cert-portal.siemens.com/productcert/pdf/ssa-501673.pdf"
        },
        {
          "url": "https://logging.apache.org/log4j/2.x/security.html"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-45105"
        },
        {
          "url": "https://security.netapp.com/advisory/ntap-20211218-0001/"
        },
        {
          "url": "https://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-apache-log4j-qRuKNEbd"
        },
        {
          "url": "https://www.debian.org/security/2021/dsa-5024"
        },
        {
          "url": "https://www.kb.cert.org/vuls/id/930724"
        },
        {
          "url": "https://www.zerodayinitiative.com/advisories/ZDI-21-1541/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "org.apache.logging.log4j/log4j-core:2.13.2"
        }
      }
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium-maven",
      "name": "gemnasium-maven",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "3.6.0"
    },
    "scanner": {
      "id": "gemnasium-maven",
      "name": "gemnasium-maven",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.27.3"
    },
    "type": "dependency_scanning",
    "start_time": "2022-03-14T04:41:15",
    "end_time": "2022-03-14T04:41:33",
    "status": "success"
  }
}
