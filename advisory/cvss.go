package advisory

import (
	"strings"

	cvss20 "github.com/pandatix/go-cvss/20"
	cvss30 "github.com/pandatix/go-cvss/30"
	cvss31 "github.com/pandatix/go-cvss/31"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

// Severity parses the given cvss vectors and returns the report.SeverityLevel value
func (a Advisory) Severity() report.SeverityLevel {
	// attempt cvss3 first
	if a.CVSSV3 != "" {
		switch {
		case strings.HasPrefix(a.CVSSV3, "CVSS:3.0"):
			cvssv3, err := cvss30.ParseVector(a.CVSSV3)
			if err == nil {
				return cvss3ScoreToSeverity(cvssv3.BaseScore())
			}
		case strings.HasPrefix(a.CVSSV3, "CVSS:3.1"):
			cvssv3, err := cvss31.ParseVector(a.CVSSV3)
			if err == nil {
				return cvss3ScoreToSeverity(cvssv3.BaseScore())
			}
		}
		// if an error is returned, we'll disregard it and try parsing the cvss2 vector next
		log.Debugf("could not parse CVSS3 vector falling back to CVSS2 for %s", a.Identifier)
	}

	// fallback to cvss2
	v, err := cvss20.ParseVector(a.CVSSV2)
	if err != nil {
		return report.SeverityLevelUnknown
	}

	return cvss2ScoreToSeverity(v.BaseScore())
}

// cvss3ScoreToSeverity uses the CVSS v3.1 "Qualitative Severity Rating Scale" to convert the score to a
// severity level.
// See https://www.first.org/cvss/v3.1/specification-document#Qualitative-Severity-Rating-Scale for more details
func cvss3ScoreToSeverity(score float64) report.SeverityLevel {
	switch true {
	case score == 0:
		return report.SeverityLevelInfo
	case score > 0 && score < 4.0:
		return report.SeverityLevelLow
	case score >= 4.0 && score < 7.0:
		return report.SeverityLevelMedium
	case score >= 7.0 && score < 9.0:
		return report.SeverityLevelHigh
	case score >= 9.0:
		return report.SeverityLevelCritical
	default:
		return report.SeverityLevelUnknown
	}
}

// cvss2ScoreToSeverity uses the CVSS v2.0 Ratings scale to convert the score to a severity level.
// See https://nvd.nist.gov/vuln-metrics/cvss for more details
func cvss2ScoreToSeverity(score float64) report.SeverityLevel {
	switch true {
	case score >= 0 && score < 4.0:
		return report.SeverityLevelLow
	case score >= 4.0 && score < 7:
		return report.SeverityLevelMedium
	case score >= 7.0:
		return report.SeverityLevelHigh
	default:
		return report.SeverityLevelUnknown
	}
}
