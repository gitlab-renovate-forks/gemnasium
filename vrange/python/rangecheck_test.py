#!/usr/bin/env python3

# run with python -m unittest rangecheck_test

import unittest
import subprocess

class TestStringMethods(unittest.TestCase):

    def test_well_formed_json_file(self):
        error_detected = False
        try:
            out = subprocess.check_output(['./rangecheck.py', 'tests/simple_in.json'], stderr=subprocess.STDOUT).decode("utf-8")
            expect = ''
            with open('tests/simple_out.json', 'r') as content_file:
                expect = content_file.read()
            self.assertEqual(out, expect)
        except subprocess.CalledProcessError as error:
            error_detected = True
        self.assertFalse(error_detected)
        self.assertEqual(out, expect)

    def test_dictionary_json_file(self):
        error_detected = False
        try:
            subprocess.check_output(['./rangecheck.py', 'tests/dictionary.json'], stderr=subprocess.STDOUT).decode('utf-8')
            self.assertTrue(False)
        except subprocess.CalledProcessError as error:
            error_detected = True
            self.assertEqual(error.returncode, 1)
        self.assertTrue(error_detected)

    def test_empty_json_file(self):
        error_detected = False
        try:
            subprocess.check_output(['./rangecheck.py', 'tests/empty.json'], stderr=subprocess.STDOUT).decode('utf-8')
            self.assertTrue(False)
        except subprocess.CalledProcessError as error:
            error_detected = True
            self.assertEqual(error.returncode, 1)

        self.assertTrue(error_detected)

    def test_non_existent_json_file(self):
        error_detected = False
        try:
            subprocess.check_output(['./rangecheck.py', 'tests/empty00.json'], stderr=subprocess.STDOUT).decode('utf-8')
            self.assertTrue(False)
        except subprocess.CalledProcessError as error:
            error_detected = True
            self.assertEqual(error.returncode, 1)
        self.assertTrue(error_detected)

    def test_gemnasium_db_advisory_ranges(self):
        error_detected = False
        try:
            out = subprocess.check_output(['./rangecheck.py', 'tests/adb_ranges_in.json'], stderr=subprocess.STDOUT).decode('utf-8')
            expect = ''
            with open('tests/adb_ranges_out.json', 'r') as content_file:
                expect = content_file.read()
            self.assertEqual(out, expect)
        except subprocess.CalledProcessError as error:
            error_detected = True
        self.assertFalse(error_detected)
