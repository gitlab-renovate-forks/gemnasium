using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NuGet.Versioning;

class Constraint
{
    [DefaultValue("")] public string range { get; set; }
    [DefaultValue("")] public string version { get; set; }

    [DefaultValue(false)]
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public bool? satisfies { get; set; }

    [DefaultValue("")]
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string error { get; set; }
}

namespace Vrange
{
    public static class VersionMatcher
    {
        public static List<VersionRange> splitIntoDisjuncts(String constraint)
        {
            // isDisjunction is basically a flag to track the semantic context in which a comma sign is used.
            // If it is used outside of brackets, it denotes a logical disjunction which would be the point
            // where we have to split the input range "[1.2, 2.4], 3.4, [3.4]" => "[1.2, 2.4]", "3.4", "[3.4]"
            var isDisjunction = true;
            var ranges = new List<VersionRange>();
            var left = 0;
            int right;

            void addDisjunct()
            {
                var range = new VersionRange();
                var disjunct = constraint.Substring(left, right - left).Trim();
                if (!VersionRange.TryParse(disjunct, out range))
                    throw new VrangeException($@"malformed range {disjunct}");

                ranges.Add(range);
            }

            for (right = 0; right < constraint.Length; right++)
            {
                switch (constraint.Substring(right, 1))
                {
                    case "[":
                    case "(":
                        left = right;
                        isDisjunction = false;
                        break;
                    case "]":
                    case ")":
                        isDisjunction = true;
                        break;
                    case ",":
                        if (!isDisjunction) continue;
                        addDisjunct();
                        left = right + 1;
                        break;
                }
            }

            addDisjunct();
            return ranges;
        }

        public static string checkVersion(String jsonIn)
        {
            var result = new List<Constraint>();
            var issues = new List<string>();
            var constraints = JsonConvert.DeserializeObject<List<Constraint>>(jsonIn, new JsonSerializerSettings
            {
                Error = delegate(object sender, ErrorEventArgs args)
                {
                    issues.Add(args.ErrorContext.Error.Message);
                    args.ErrorContext.Handled = true;
                }
            });

            if (jsonIn.Trim().Length == 0 || issues.Count > 0)
                throw new VrangeException("Invalid JSON");

            foreach (var constraint in constraints)
            {
                // version constraint solution
                var solution = new Constraint {range = constraint.range, version = constraint.version};

                // split range into disjuncts and parse them
                var ranges = new List<VersionRange>();
                try
                {
                    ranges = splitIntoDisjuncts(constraint.range);
                }
                catch (VrangeException e)
                {
                    solution.error = e.Message;
                    solution.satisfies = null;
                    result.Add(solution);
                    continue;
                }

                // parse version
                var version = new NuGetVersion("0");
                if (!NuGetVersion.TryParse(constraint.version, out version))
                {
                    solution.error = "malformed version";
                    solution.satisfies = null;
                    result.Add(solution);
                    continue;
                }

                solution.satisfies = ranges.Any(r => r.Satisfies(version));
                result.Add(solution);
            }

            return JsonConvert.SerializeObject(result);
        }
    }
}