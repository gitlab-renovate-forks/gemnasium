package gem

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/cli"

func init() {
	Register("gem")
}

// Register registers the given resolver to use the gem resolver
func Register(resolverName string) {
	cli.Register(resolverName, "gem/vrange.rb")
}
